/*
Navicat MySQL Data Transfer

Source Server         : 公司开发
Source Server Version : 50639
Source Host           : 172.16.8.187:3307
Source Database       : jinkai

Target Server Type    : MYSQL
Target Server Version : 50639
File Encoding         : 65001

Date: 2020-01-03 22:22:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for base_dictionary
-- ----------------------------
DROP TABLE IF EXISTS `base_dictionary`;
CREATE TABLE `base_dictionary` (
  `id` varchar(36) NOT NULL COMMENT '主键',
  `code` varchar(50) NOT NULL COMMENT '字典编码',
  `name` varchar(50) NOT NULL COMMENT '字典名称',
  `type_code` varchar(50) NOT NULL COMMENT '字典类型编码',
  `type_name` varchar(50) NOT NULL COMMENT '字典类型名称',
  `deleted` bit(1) NOT NULL COMMENT '删除状态',
  `status` bit(1) NOT NULL COMMENT '使用状态  0 禁用  1 启用',
  `system_status` bit(1) DEFAULT NULL COMMENT '是否为系统字典 0 否 1是',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `create_time` datetime NOT NULL COMMENT '数据创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '数据更新时间',
  `create_by` varchar(36) DEFAULT NULL COMMENT '创建人',
  `update_by` varchar(36) DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据字典表';

-- ----------------------------
-- Table structure for business_log
-- ----------------------------
DROP TABLE IF EXISTS `business_log`;
CREATE TABLE `business_log` (
  `id` varchar(36) NOT NULL COMMENT '主键id',
  `menu` varchar(36) DEFAULT NULL COMMENT '菜单',
  `button` varchar(36) DEFAULT NULL COMMENT '按钮',
  `target_name` varchar(200) DEFAULT NULL COMMENT '目标类',
  `method_name` varchar(200) DEFAULT NULL COMMENT '方法名称',
  `host` varchar(36) DEFAULT NULL COMMENT '操作ip',
  `request_params` varchar(500) DEFAULT NULL COMMENT '请求参数',
  `response_params` text COMMENT '返回参数',
  `result` varchar(12) DEFAULT NULL COMMENT '操作结果 失败 成功',
  `request_time` datetime DEFAULT NULL COMMENT '请求时间',
  `response_time` datetime DEFAULT NULL COMMENT '相应时间',
  `exception` text COMMENT '异常描述',
  `description` varchar(200) DEFAULT NULL COMMENT '操作描述',
  `operation_user` varchar(36) DEFAULT NULL COMMENT '操作人',
  `operation_type` varchar(36) DEFAULT NULL COMMENT '操作类型 ',
  `create_time` datetime DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`),
  KEY `idx_menu` (`menu`) USING BTREE,
  KEY `idx_button` (`button`) USING BTREE,
  KEY `idx_method` (`method_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='业务日志表';

-- ----------------------------
-- Table structure for fms_pay_order
-- ----------------------------
DROP TABLE IF EXISTS `fms_pay_order`;
CREATE TABLE `fms_pay_order` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `order_no` varchar(36) NOT NULL COMMENT '订单号',
  `product_name` varchar(36) NOT NULL COMMENT '商品名称',
  `description` varchar(255) NOT NULL COMMENT '商品描述',
  `out_trade_no` varchar(36) NOT NULL COMMENT '商户订单号',
  `transaction_id` varchar(36) DEFAULT NULL COMMENT '流水号-支付订单号',
  `nonce_str` varchar(36) DEFAULT NULL COMMENT '随机字符串',
  `pay_user` varchar(36) DEFAULT NULL COMMENT '缴费人',
  `phone` varchar(36) DEFAULT NULL COMMENT '手机号码',
  `qr_code_url` varchar(255) NOT NULL COMMENT '支付二维码路径',
  `pay_fee` decimal(12,2) NOT NULL COMMENT '缴费金额',
  `pay_type` int(2) NOT NULL COMMENT '缴费方式 1 微信 2 支付宝',
  `pay_status` int(2) NOT NULL COMMENT '支付状态  0 下单 1 支付中 2 支付失败  3 支付成功 ',
  `order_refund_status` bit(1) NOT NULL COMMENT '0 未退款 1 已退款',
  `refund_fee` decimal(12,2) NOT NULL COMMENT '退费金额',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_by` varchar(36) NOT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(36) DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_order_no` (`order_no`) USING BTREE,
  UNIQUE KEY `uniq_out_trade_no` (`out_trade_no`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='支付订单';

-- ----------------------------
-- Table structure for fms_refund_order
-- ----------------------------
DROP TABLE IF EXISTS `fms_refund_order`;
CREATE TABLE `fms_refund_order` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `pay_id` bigint(11) NOT NULL COMMENT '支付id',
  `out_refund_no` varchar(36) NOT NULL COMMENT '商户退款单号',
  `refund_fee` decimal(12,2) NOT NULL COMMENT '退款金额',
  `refund_status` bit(1) DEFAULT NULL COMMENT '退款状态 0 失败  1 成功',
  `refund_user` varchar(36) NOT NULL COMMENT '退款人',
  `refund_reason` varchar(200) NOT NULL COMMENT '退款事由',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_by` varchar(36) NOT NULL COMMENT '创建人',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_out_refund_no` (`out_refund_no`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='订单退费明细';


DROP TABLE IF EXISTS `schedule_config`;
CREATE TABLE `schedule_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `job_name` varchar(50) DEFAULT NULL COMMENT '定时任务名称',
  `class_name` varchar(200) DEFAULT NULL COMMENT '类名称',
  `method` varchar(50) DEFAULT NULL COMMENT '方法',
  `cron` varchar(50) DEFAULT NULL COMMENT 'cron 表达式',
  `enabled` bit(1) DEFAULT b'1' COMMENT '状态:1正常,0停用',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(36) DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(36) DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='定时任务表配置';


DROP TABLE IF EXISTS `schedule_job_log`;
CREATE TABLE `schedule_job_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `job_name` varchar(50) DEFAULT NULL COMMENT '任务名称',
  `start_time` datetime DEFAULT NULL COMMENT '开始执行时间',
  `end_time` datetime DEFAULT NULL COMMENT '执行结束时间',
  `cost_seconds` int(11) DEFAULT NULL COMMENT '花费时间',
  `result` varchar(12) DEFAULT NULL COMMENT '操作结果 失败 成功',
  `exception` text COMMENT '异常信息',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(36) DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=921 DEFAULT CHARSET=utf8 COMMENT='定时任务执行日志';

DROP TABLE IF EXISTS `es_doc`;
CREATE TABLE `es_doc` (
  `doc_id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_name` varchar(512) DEFAULT NULL COMMENT '文档名称',
  `doc_size` bigint(20) DEFAULT NULL COMMENT '文档大小',
  `doc_sha256` varchar(256) DEFAULT NULL COMMENT '文档sha256',
  `doc_create_date` datetime DEFAULT NULL COMMENT '文档创建时间',
  `doc_user_id` int(11) DEFAULT NULL COMMENT '文档创建人',
  `doc_file_id` varchar(128) DEFAULT NULL COMMENT '文件id',
  `doc_open` smallint(6) DEFAULT NULL,
  `doc_type` varchar(128) DEFAULT NULL COMMENT '文档类型',
  `doc_title` varchar(512) DEFAULT NULL COMMENT '文档标题',
  `doc_content` longtext COMMENT '文档内容',
  `doc_delete` smallint(6) DEFAULT NULL COMMENT '文档删除',
  `doc_modify_date` datetime DEFAULT NULL COMMENT '文档修改时间',
  `doc_index` tinyint(4) DEFAULT NULL COMMENT '文档索引',
  `source` varchar(64) DEFAULT NULL,
  `source_url` varchar(1024) DEFAULT NULL,
  `doc_status` tinyint(4) DEFAULT '0' COMMENT '文档状态',
  `doc_convert` tinyint(4) DEFAULT '0' COMMENT '文档转换',
  PRIMARY KEY (`doc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='es 文档索引';

DROP TABLE IF EXISTS `es_doc_search_record`;
CREATE TABLE `es_doc_search_record` (
  `search_record_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '搜索id',
  `search_keyword` varchar(256) NOT NULL COMMENT '搜索关键词',
  `doc_id` int(11) NOT NULL COMMENT '搜索后阅读了哪篇文档',
  `user_id` int(11) NOT NULL COMMENT '搜索人',
  PRIMARY KEY (`search_record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='es 文档搜索';

DROP TABLE IF EXISTS `es_share_doc`;
CREATE TABLE `es_share_doc` (
  `share_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '分享id',
  `share_to` int(11) NOT NULL COMMENT '分享给哪个用户',
  `share_date` datetime NOT NULL COMMENT '分享日期',
  `doc_id` int(11) NOT NULL COMMENT '文档id',
  `valid_time` int(11) NOT NULL DEFAULT '0' COMMENT '文档有效期，单位秒，0表示无限',
  PRIMARY KEY (`share_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='es 文档分享';

DROP TABLE IF EXISTS `es_upload_file`;
CREATE TABLE `es_upload_file` (
  `file_id` varchar(64) NOT NULL,
  `file_path` varchar(512) DEFAULT NULL COMMENT '文件路径',
  `sha256` varchar(255) DEFAULT NULL COMMENT '文件sha256',
  `file_size` int(12) DEFAULT NULL COMMENT '文件大小',
  `file_type` varchar(256) DEFAULT NULL COMMENT '文件类型',
  `file_desc` varchar(1024) DEFAULT NULL COMMENT '文件描述',
  `file_name` varchar(1024) DEFAULT NULL COMMENT '文件名',
  `original_file_name` varchar(1024) DEFAULT NULL COMMENT '原始文件名',
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='es 文档上传';

DROP TABLE IF EXISTS `sys_email`;
CREATE TABLE `sys_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `receive_email` varchar(255) NOT NULL COMMENT '发送到邮件地址',
  `subject` varchar(64) NOT NULL COMMENT '主题',
  `content` text NOT NULL COMMENT '内容',
  `attach_files` varchar(255) DEFAULT NULL COMMENT '附件地址',
  `send_time` datetime DEFAULT NULL COMMENT '发送时间',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_by` varchar(32) NOT NULL COMMENT '创建人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='邮件发送';